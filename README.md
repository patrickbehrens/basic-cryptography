Patrick Behrens

COMP 447 - Online

#Project #2

This repository contains a simple implementation of the RSA algorithm. It generates a public and private key and stores them to a file. The public is used to encrypt the message, and private key for decypting. 


I decided to try an implement RSA because I wanted to get a better understanding of how asymmetric encryption works. At work I use keys generated with RSA every day, but still didn't understand some of the finer details. 

##Usage

### Requirements
Python 2.7 and the `pyprimes` library is needed to run this. You can install `pyprimes` by running:

`pip install pyprimes`

### Key Generation
To generate a key-pair that is printed to the screen use the `-g` flag without an argument. The default key size is 18 bits. You can specify an other key length by using `-l` and passing a length in bits. Unfortunately, it takes a long time to decrypt things with keys larger than 20 bits because I did not implement the Chinese Remainder theorem. 18 bits works well on my computer for small messages. Also, Python isn't the fastest.

Use: 

`python beagle-crypt.py -g`

To generate a key-pair file that contains both public and secret/private keys use `-g`	and `-o` followed by the output file name.

Use: 

`python beagle-crypt.py -g -o key-pair.txt`

To output a file with contents similar to this:

`p:65537,s:201185,m:221917`

Use: 

`python beagle-crypt.py -g -l 32 -o key-pair.txt`

To output a 32 bit key.

Where p is the public key number, s is the secret key number, and m is the modulus number.

### Encryption
To encrypt a file using the key-pair file use `-e` with the message text file as a parameter. Use the `-k` parameter to sepcify a key file, otherwise a default one will be used. Adding `-o` with an output filename will outpfut the cipher text to the specifed file. Otherwise it will print it out on the console. :

`python beagle-crypt.py -e test-msg.txt -k keypair.txt -o cipher.txt`

If `test-msg.txt` was `foobar` the output might look something like this:

`76716,47809,47809,53781,43486,195968,209232`

### Decryption
To decrypt a file use `-d` with the cipher text file. Specify a key with `-k` and an output with `-o`:

`python beagle-crypt.py -d cipher.txt -k keypair.txt -o plain.txt`


##Design
Choosing to implement RSA was a bit more work than I expected, so this script has very basic functions. It can:

* Generate an key-pair file
* Encrypt a file using the public key portion of the key-pair file
* Output cipher text to a file of your choosing
* Decrypt a cipher text file using the secret key portion of the key-pair file
* Make your laptop very warm when passed a large key size

Python was used for this project because there are some nice libraries that can help with math functions, and it is a language I use often. The choice of Python made some of the bit/byte level operations a bit harder or less clear unfortunately. Also, the performance would improve greatly if a more efficient language was used. Because I don't know much about bit/byte level Python it was kind of hard to generate the cipher text in an optimal format. 

Currently, each "character" in the outputted cipher text is represented by an integer separated by commas instead of a string of ascii character's. The numbers stored are way bigger than any thing that could be mapped to a ascii char representation. I didn't have enough time to write code that breaks down each large integer into 8 bit chunks. Using UTF-8 would have made this easier because each char would be 32 bits instead of 8.  

The key-pair file contains both the public and private/secret. It also contains the modulus number used. These could be broken out into tuples for each type of key. But to make to easier all the info is stored in one file.

* Public (n, e)
* Secret (n, d) 

My method for generating primes isn't that good. Maybe a sieve of some sort would have been better. I decided to create primes by getting random bit's of a certain length then checking if it the resulting number was prime. If it wasn't an other random number was chosen. It uses the Miller-Rabin primality test provided by pyprimes.



##Resources Used

The most helpful resource was the website of a computer consulting company in Australia. They explained the RSA algorithm very nicely. Before I started coding I did a few exercises with pen and paper that were provided on the site. Their breakdown of RSA can be here:

[RSA Algorithm](http://www.di-mgt.com.au/rsa_alg.html)

Wikibooks was also used for the Python implementation's of the Extended Euclidean Algorithm

[Extended Euclidean Algorithm](https://en.wikibooks.org/wiki/Algorithm_Implementation/Mathematics/Extended_Euclidean_algorithm)

PyPrimes was used for its' excellent implementation of the Miller-Rabin Primality Test

[PyPrimes](https://pypi.python.org/pypi/pyprimes/)
