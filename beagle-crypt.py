import sys, getopt
import math
import random
import time
from pyprimes import isprime


"""
Functions for printing out data
"""

def print_num(number, display=True):
    """
    Print the number prettily
    """
    num_str = "%.0f" % (number)
    if display:
        print num_str
    return num_str

def print_array(array):
    """
    Attempt to print out a string by coercing each element into a string
    """
    print ''.join([str(el) for el in array])

def print_bytes(byte_array):
    for b in byte_array:
        print_num(b)

def print_key_pair(pk, sk, mod, display=True):
    """
    Prints out the key pair real nice like and returns formatted strings
    """
    sk = print_num(sk, False)
    pk = print_num(pk, False)
    mod = print_num(mod, False)
    output = ''
    output = output + 'p:{0},'.format(pk)
    output = output + 's:{0},'.format(sk)
    output = output + 'm:{0}'.format(mod)
    if display:
        print "Secret key (sk): {0}".format(sk)
        print "Public key (pk): {0}".format(pk)
        print "Modulus (mod): {0}".format(mod)
    return output


"""
Functions for converting arrays
"""

def long_array_to_string(byte_array):
    """
    Returns a string from a given array of longs using fancy one-liner
    """
    return ''.join([chr(char) for char in byte_array])

def csv_version(byte_array):
    return ','.join([print_num(char, False) for char in byte_array])


def ccsv_to_byte_array(ccsv):
    """
    Converts a string like this "76716,47809,47809" into an array of longs
    """
    str_array = ccsv.split(',')
    byte_array = [long(string) for string in str_array]
    return byte_array


"""
File and utility functions
"""

def byte_size(num):
    """
    Returns length of number in bytes
    """
    return sys.getsizeof(num)

def read_file_text(file_path):
    """
    Given a file path
    Return a string
    """
    content = ''
    with open(str(file_path)) as f:
        content = f.readlines()

    return '\n'.join([line for line in content])
    # f = open(file_path, 'r')
    # text = f.read()
    # print text
    # return f.read()

def write_text_to_file(file_path, text):
    """
    Write given text to a file
    """
    f = open(file_path,'w')
    f.write(text)
    f.close()
    return True

def parse_keys_from_string(key_string):
    """
    Parses key values from strings of format 
    p = public key
    s = secret key
    m = modulus number
    p:65537,s:63073,m:240199
    """
    split_str = key_string.split(',')
    pk = int(split_str[0].split(':')[1])
    sk = int(split_str[1].split(':')[1])
    mod = int(split_str[2].split(':')[1])
    return pk, sk, mod

def convert_keys_to_file_format(pk, sk, mod):
    """
    Returns a string that conforms to the file format
    p = public key
    s = secret key
    m = modulus number
    p:65537,s:63073,m:240199
    """
    return 'p:{0},s:{1},m:{2}'.format(pk, sk, mod)

"""
Encryption related functions
"""

def get_prime(n):
    """
    Returns a (probably) prime number of bit-length n
    """
    prime = random.getrandbits(n)
    while not isprime(prime):
        prime = random.getrandbits(n)
    return prime

def choose_e(mod):
    """
    Choose between 3 or 65537 for the value of e
    This is because they are prime and make modular exponentiation easier
    """
    if mod < 65537:
        return 3
    else:
        return 65537

def egcd(a, b):
    """
    Returns the Euclidean greatest common denominator
    """
    x,y, u,v = 0,1, 1,0
    while a != 0:
        q, r = b//a, b%a
        m, n = x-u*q, y-v*q
        b,a, x,y, u,v = a,r, u,v, m,n
    gcd = b
    return gcd, x, y

def mod_inv(a, m):
    """
    Computes the secret exponent using multiplicative modular inversion
    d = e^-1 mod phi
    This function and egcd() were taken from 
    https://en.wikibooks.org/wiki/Algorithm_Implementation/Mathematics/Extended_Euclidean_algorithm
    """
    gcd, x, y = egcd(a, m)
    if gcd != 1:
        return None  # modular inverse does not exist
    else:
        return x % m

def calc_sk(e, phi):
    """
    Computes the secret exponent using multiplicative modular inversion
    d = e^-1 mod phi
    This function and egcd() were taken from 
    https://en.wikibooks.org/wiki/Algorithm_Implementation/Mathematics/Extended_Euclidean_algorithm
    """
    return mod_inv(e, phi)


def gen_mod(key_len):
    """
    Generate the modulus (mod) with a length of key_len.
    mod = p*q 
    The length of n in bits should equal key_len  
    """

    bit_count = False
    p_len = key_len
    p = get_prime(p_len)
    q = get_prime(p_len)
    # decrement the bit length of the primes until a suitable mod value is found
    while not bit_count:
        # make sure they arent the same
        if p != q:
            # calc the mod value
            mod = p * q
            # if mod os desired length stop
            if mod.bit_length() == key_len:
                bit_count = True
            elif mod.bit_length() < key_len/4 and key_len >32:
                # if p_len gets too small reset and try again
                p_len = key_len
                p = get_prime(p_len)
                q = get_prime(p_len)
            elif mod.bit_length()  < key_len:
                # for small keys this is needed
                p_len = key_len
                p = get_prime(p_len)
                q = get_prime(p_len)
            else:
                # decrement p_len until a good size is found
                p_len -= 1
                q = get_prime(p_len)
                p = get_prime(p_len)
    print "prime q: " + str(q)
    print "prime p: " + str(p)
    return mod, p, q


def gen_pair(p, q, mod, key_len):
    """
    Generates key pair values for a key of a given length.
    """
    pk = e = choose_e(mod)
    # calc phi from p and q
    phi = ((p-1)*(q-1))
    # take the modular inverse of e and phi to get the secret key
    sk = mod_inv(e, phi)
    return pk, sk, mod, p, q


def encrypt_text(pk, mod, text):
    """
    Plain text represented as an integer m , 1 < m < n
    cipher text, c = m^pk %% mod
    returns an array of longs
    """
    byte_array = []
    # for each char in the text
    for char in text:
        # turn it into a integer representation
        char_int = ord(char)
        # then encrypted it using c = m^pk %% mod
        c_char = (char_int ** pk) % mod
        byte_array.append(c_char)   
    return byte_array


def decrypt_text(sk, mod, ciphertext):
    """
    Decrypt the cipher text. This takes a long time for keys > 24 bits
    Might try to use the Chinese Remainder Theorem if I have time
    It was thoruughly cocnfusing though
    http://www.di-mgt.com.au/crt.html
    """

    long_array = []
    # for each char in the cipher text
    for char in ciphertext:
        # decrypt using c^d % n into correct intenger
        c_char = long((char ** sk) % mod)

        long_array.append(c_char)
    return long_array



def main(argv):
    pub_key = 65537
    sec_key  = 181025
    key_len = 18
    mod = 232399
    in_file = False
    plain_input = "foobar"
    cipher_input = "76716,47809,47809,53781,43486,195968"
    out_file = False
    test_string = "foo"
    gen_key = False
    encrypt = False
    decrypt = False
    verbose = False
    interactive = False
    key_file = None
    sample_key_string = "p:65537,s:181025,m:232399"

    try:
        opts, args = getopt.getopt(argv,"hgve:p:s:l:m:i:o:c:d:k:",
            ["pubkey=","seckey=", "keylen=", "mod=", "in=", "out=", "generate=", "verbose=", "interactive=", "decrypt=", "encrypt=", "keyfile="])
    except getopt.GetoptError:
        print "Some sort of failure with the args :("
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print "To generate a key pair"
            print 'rsa.py -g <outputfile> -k <keylen>'
            print "To encrypt a file"
            print 'rsa.py -p <publickey> -m <mod> -i <inputfile> -o <outputfile> -v -c'
            print "To decrypt a file"
            print 'rsa.py -s <secretkey> -m <mod> -i <inputfile> -o <outputfile> -v -c'
            sys.exit()
        elif opt in ("-p", "--pubkey"):
            pub_key = arg
        elif opt in ("-s", "--seckey"):
            sec_key = arg
        elif opt in ("-l", "--keylen"):
            key_len = int(arg)
        elif opt in ("-m", "--mod"):
            mod = arg
        elif opt in ("-i", "--in"):
            in_file = arg
        elif opt in ("-o", "--out"):
            out_file = arg
        elif opt in ("-k", "--key-file"):
            key_file = arg
        elif opt in ("-g", "--generate"):
            gen_key = True
        elif opt in ("-d", "--decrypt"):
            decrypt = True
            if arg:
                in_file = arg
        elif opt in ("-e", "--encrypt"):
            encrypt = True
            if arg:
                in_file = arg
        elif opt in ("-v", "--verbose"):
            verbose = True

    if key_file:
        key_file_text = read_file_text(key_file)
        pub_key, sec_key, mod = parse_keys_from_string(key_file_text)
    else:
      pub_key, sec_key, mod = parse_keys_from_string(sample_key_string)  

    # If the -g flag is used generate a new pair of keys
    if gen_key:
        # genreate the modulus number
        mod, p, q = gen_mod(key_len)
        # generate the key pair
        pk, sk, mod, p, q = gen_pair(p, q, mod, key_len)
        if verbose:
            print "Mod has been genreated"
            print "p: {0}".format(print_num(p, False))
            print "The length in bits  of p is {0}".format(p.bit_length())
            print "q: {0}".format(print_num(q, False))
            print "The length of in bits  q is {0}".format(q.bit_length())
            print "mod: {0}".format(print_num(mod, False))
            print "The length in bits of mod is {0}".format(mod.bit_length())
            print "Public key is: {0}".format(print_num(pk, False))
            print "The length in bits of public key is {0}".format(sk.bit_length())   
            print "Secret key is: {0}".format(print_num(sk, False))
            print "The length in bits  of secret key is {0}".format(sk.bit_length())
        else:
            print "Here are the key values you need"
            print "Mod: {0}".format(print_num(mod))
            print "Public Key: {0}".format(print_num(pk))
            print "Secret Key: {0}".format(print_num(sk))
        
        if out_file:
            write_text_to_file(out_file, convert_keys_to_file_format(pk, sk, mod))
            print print_key_pair(pk, sk, mod)

    elif encrypt and pub_key and mod:
        print "Starting to encrypt"
        # if there is a public key and mod number given encrypt based on input type
        if in_file or (in_file and out_file):
            print 'Input file specified as: {0}'.format(in_file)
            if out_file:
                print 'Output file specified as: {0}'.format(out_file)
            plain_input = read_file_text(in_file)

        elif interactive:
            plain_input = raw_input("Enter the text you want to encrypt: ")
        
        byte_array = encrypt_text(pub_key, mod, plain_input)
        if out_file:
            write_text_to_file(out_file, csv_version(byte_array))
            print "An ecnrypted csv version of the file has been outputed"
            pass
        else:
            print "comma delimited number text is"
            print csv_version(byte_array)

    elif decrypt and sec_key and mod:
        print "Starting to decrypt"
        # if there is a secret key and mod number given, decrypt based on input type
        if in_file or (in_file and out_file):
            print 'Input file specified as: {0}'.format(in_file)
            if out_file:
                print 'Output file specified as: {0}'.format(out_file)
            cipher_input = read_file_text(in_file)
            pass
        elif interactive:
            cipher_input = raw_input("Enter a CSV version of the cipher text (): ")


        # convert CSV into an array of longs
        long_array = ccsv_to_byte_array(cipher_input)
        # decrypt into 
        plain_byte_array = decrypt_text(sec_key, mod, long_array)
        

        if out_file:
            write_text_to_file(out_file, long_array_to_string(plain_byte_array))
            print "An ecnrypted csv version of the file has been outputed"
            pass
        else:
            print "Plain Text Is"
            print '-----------------------------'
            print long_array_to_string(plain_byte_array)

    else:
        print "Testing each function"
        mod, p, q = gen_mod(key_len)
        print "Mod has been genreated"
        print "p: {0}".format(print_num(p, False))
        print "The length of p is {0}".format(p.bit_length())
        print "q: {0}".format(print_num(q, False))
        print "The length of q is {0}".format(q.bit_length())
        print "mod: {0}".format(print_num(mod, False))
        print "The length of mod is {0}".format(mod.bit_length())

        pk, sk, mod, p, q = gen_pair(p, q, mod, key_len)
        print "sk: "
        print sk
        print "pk: "
        print pk
        print "mod: "
        print mod

        print "Starting encryption ________________________________"
        ctext = encrypt_text(pk, mod, test_string)
        print "This is the cypher text"
        print ctext
        print type(ctext[0])

        print "Starting decryption ________________________________"
        ptext = decrypt_text(sk, mod, ctext)
        print "This is the plain text"
        print ptext


if __name__ == "__main__":
   main(sys.argv[1:])